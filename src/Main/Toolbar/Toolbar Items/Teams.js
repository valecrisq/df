import React from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';


export default class Shots extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 1,
        };
    }

    handleChange = (event, index, value) => this.setState({value});

    render() {
        return (
            <div>
                <DropDownMenu labelStyle={{color: 'whitesmoke'}} value={this.state.value}
                              onChange={this.handleChange}>
                    <MenuItem value={1} primaryText='Trending Teams'/>
                    <MenuItem value={2} primaryText='Popular Teams'/>
                    <Divider/>
                    <MenuItem value={3} primaryText='Add Your Design Team'/>
                </DropDownMenu>
            </div>
        );
    }
}