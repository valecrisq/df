import React from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';


export default class Shots extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 1,
        };
    }

    handleChange = (event, index, value) => this.setState({value});

    render() {
        return (
            <div>
                <DropDownMenu labelStyle={{color: 'whitesmoke'}} value={this.state.value}
                              onChange={this.handleChange}>
                    <MenuItem value={1} primaryText='Blog'/>
                    <MenuItem value={2} primaryText='Podcast'/>
                    <MenuItem value={3} primaryText='Meetups'/>
                    <MenuItem value={4} primaryText='Hang Time Seattle'/>
                    <Divider/>
                    <MenuItem value={5} primaryText='Host a Meetup'/>
                </DropDownMenu>
            </div>
        );
    }
}