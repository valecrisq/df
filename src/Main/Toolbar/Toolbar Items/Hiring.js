import React from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';


export default class Shots extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 1,
        };
    }

    handleChange = (event, index, value) => this.setState({value});

    render() {
        return (
            <div>
                <DropDownMenu labelStyle={{color: 'whitesmoke'}} value={this.state.value}
                              onChange={this.handleChange}>
                    <MenuItem value={1} primaryText='Hiring at Dribbble'/>
                    <Divider/>
                    <MenuItem value={2} primaryText='Post a Job'/>
                    <MenuItem value={3} primaryText='Scout Designers'/>
                    <MenuItem value={4} primaryText='Add Your Design Team'/>
                    <MenuItem value={5} primaryText='Hire on Crew'/>
                </DropDownMenu>
            </div>
        );
    }
}