import React from 'react';
import dribblelogo from '../../image/dribblelogo.png';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import Shots from './Toolbar Items/Shots';
import Designers from './Toolbar Items/Designers';
import Teams from "./Toolbar Items/Teams";
import Community from "./Toolbar Items/Community";
import Jobs from "./Toolbar Items/Jobs";
import Hiring from "./Toolbar Items/Hiring";


export default class SearchBar extends React.Component {


    render() {
        return (
            <Toolbar className={'toolbar'}>
                <ToolbarGroup>
                    <img src={dribblelogo} alt=""/>

                        <Shots value={this.props.value}
                               handleChange={this.props.handleChange}
                        />

                        <Designers value={this.props.value}
                               handleChange={this.props.handleChange}
                        />

                        <Teams value={this.props.value}
                               handleChange={this.props.handleChange}
                        />

                        <Community value={this.props.value}
                               handleChange={this.props.handleChange}
                        />

                        <Jobs value={this.props.value}
                               handleChange={this.props.handleChange}
                        />

                        <Hiring value={this.props.value}
                               handleChange={this.props.handleChange}
                        />

                </ToolbarGroup>
            </Toolbar>
        );
    }
}
