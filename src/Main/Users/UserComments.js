import React from 'react';
import {Card, CardHeader} from 'material-ui/Card'
import Avatar from 'material-ui/Avatar';


export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
        }
    }


    componentDidMount() {
        fetch('https://randomuser.me/api/?results=8')
            .then(response => response.json())
            .then(responsejson => {
                this.setState({
                    users: responsejson
                })
            })
    }


    render() {

        if (this.state.users.length < 1) return false;

        const followers = this.state.users.results.map((user, index) =>
            <Card className={'user-card'} key={index}>
                <CardHeader style={{display: 'flex', width: '100%'}}>
                    <img style={{borderRadius: '100%', marginLeft: -90}} src={user.picture.thumbnail} alt="" />
                    <h3 className={'user-name'}>
                        {user.name.first + ' ' + user.name.last}
                    </h3>
                    <h5 className={'user-info'}>{'Email: ' + user.email + ' - Username: ' + user.login.username}</h5>
                </CardHeader>
                <span className={'user-comment'}>{'"' + user.location.street + ' ' + user.location.city + ' ' + user.login.password + '!"'}</span>
            </Card>
        );


        return (
            <div>
                {followers}
            </div>
        );
    }
}