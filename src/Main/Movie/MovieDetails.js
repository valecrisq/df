import React from 'react';
import Divider from 'material-ui/Divider';
import {List, ListItem} from 'material-ui/List';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ActionVisibility from 'material-ui/svg-icons/action/visibility';
import SocialShare from 'material-ui/svg-icons/social/share';


export default class MovieDetails extends React.Component {
    render() {

        const movieDetails =
            <div>
                <List>
                    <ListItem primaryText="Like?" secondaryText="247 likes" leftIcon={<ActionFavorite/>}/>
                    <Divider/>
                    <ListItem primaryText="Starred" leftIcon={<ActionGrade/>}/>
                    <Divider/>
                    <ListItem primaryText="Views" leftIcon={<ActionVisibility/>}/>
                    <Divider/>
                    <ListItem
                        primaryText="Share"
                        leftIcon={<SocialShare/>}
                    />
                </List>
                <div className={'movie-info'}>
                    <h1>{this.props.data.Title}</h1>
                    <h4>Year: {this.props.data.Year}</h4>
                    <h4>Awards: {this.props.data.Awards}</h4>
                    <h4>Running time: {this.props.data.Runtime}</h4>
                    <h4>Director: {this.props.data.Director}</h4>
                    <h4>Genre: {this.props.data.Genre}</h4>
                    <h4>Actors: {this.props.data.Actors}</h4>

                </div>
            </div>;

        return (
            <div className={'menu'}>
                {movieDetails}
            </div>
        );
    }
}
