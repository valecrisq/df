import React from 'react';
import lebron from '../../image/lebron.png'
import {Card, CardHeader} from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import MovieDetails from "./MovieDetails";

export default class FilmCard extends React.Component {
    render() {

        const cardUser =
            <Card>
                <CardHeader className={'card-header'}
                            title="Movie Listings"
                            subtitle="by Lebron James for PLATFORM on May 11, 2018"
                            avatar={
                                <Avatar className={'card-avatar'}
                                        src={lebron}
                                />
                            }
                />
            </Card>;

        const cardMovie =
            <Card>
                <div style={{display: 'flex', marginTop: 10}}>
                    <div style={{backgroundImage: 'url(' + this.props.data.Poster + ')'}}
                         className={'movie-image'}>
                    </div>
                    <MovieDetails
                        data={this.props.data}
                    />
                </div>
            </Card>;

        const moviePlot =
            <div className={'movie-info'}>
                <h2>Plot: </h2>
                <h3>{this.props.data.Plot}</h3>
            </div>;

        return (
            <div>
                {cardUser}
                {cardMovie}
                {moviePlot}
            </div>
        );
    }
}