import React from 'react';
import FilmCard from './Movie/FilmCard';
import SearchBar from './Toolbar/SearchBar'
import Divider from 'material-ui/Divider';
import UserComments from './Users/UserComments'

export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }

}

    componentDidMount() {
        fetch('http://www.omdbapi.com/?i=tt3896198&apikey=7ac3ae1a&t=Jumanji&y=1995&plot=full')
            .then(response => response.json())
            .then(responsejson => {
                this.setState({
                    data: responsejson
                })
            });
    }



    render() {

        if (this.state.data.length < 1) return false;

        return (
            <div>
                <SearchBar/>
                <div className={'film-card'}>
                    <FilmCard
                        data={this.state.data}
                    />
                    <Divider/>
                    <UserComments/>
                </div>
            </div>
        );
    }
}