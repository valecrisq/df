import React from 'react';
import Main from './Main/Main';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export default class App extends React.Component {
    render() {
        return (
            <MuiThemeProvider className={'app'}>
                <Main/>
            </MuiThemeProvider>
        );
    }
}
